#include "CryptoAnalyzer.hpp"
#include "msg/SearchRequest.pb.h"
#include <predi/core/Core.hpp>
#include <iostream>

int main()
{
    auto ca = CryptoAnalyzer{};
    std::cout << ca.complexity() << std::endl;
    ca.analyze();
    SearchRequest s{};
    s.clear_query();

    return EXIT_SUCCESS;
}
