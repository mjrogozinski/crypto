#pragma once

#include <gmock/gmock.h>

#include <cstdlib>
#include <functional>

struct SystemMock
{
    SystemMock();

    friend int ::system(const char*);
    using system_t = decltype(::system);

    MOCK_METHOD1(system, system_t);

private:
    static std::function<system_t> callSystem;
};
