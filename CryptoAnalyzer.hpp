#pragma once

struct CryptoAnalyzer
{
    float complexity() const;
    void analyze() const;
};
